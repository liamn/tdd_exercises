class BowlingGame:

    def __init__(self):
        self.frames = []
        
    def roll(self, pins):
        if not self.frames or len(self.frames[-1]) == 2:
            if len(self.frames) == 10:
                self.frames[-1].append(pins)
            else:
                self.frames.append([pins])
                if pins == 10:
                    self.frames[-1].append(0) # strike is [10, 0]
        else:
            self.frames[-1].append(pins)

    def score(self):
        total = 0
        print(self.frames)
        for i, frame in enumerate(self.frames):
            if sum(frame) == 10:
                if frame[0] == 10:
                    if self.frames[i+1][0] == 10:
                        if i == 9:
                            total += sum(self.frames[i])
                        elif i == 8:
                            total += sum(self.frames[i+1])
                        else:
                            total += 10 + self.frames[i+1][0] + self.frames[i+2][0]
                    else:
                        total += 10 + sum(self.frames[i+1])
                else:
                    total += 10 + self.frames[i + 1][0]
            else:
                total += sum(frame)
        return total

