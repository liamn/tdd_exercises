from unittest import TestCase

from bowling_game import BowlingGame

class TestBowlingGame(TestCase):
    
    def setUp(self):
        self.game = BowlingGame()
    
    def roll_many(self, num_rolls, pins):
        for roll in range(num_rolls):
            self.game.roll(pins)

    def test_gutter_game(self):
        self.roll_many(20, 0)
        self.assertEqual(self.game.score(), 0)

    def test_all_ones_game(self):
        self.roll_many(20, 1)
        self.assertEqual(self.game.score(), 20)

    def test_mixed_rolls(self):
        self.roll_many(10, 2)
        self.roll_many(10, 3)
        self.assertEqual(self.game.score(), 50)
    
    def test_one_spare(self):
        self.game.roll(7)
        self.game.roll(3) # 7 + 3 = 10 = a spare
        self.roll_many(18, 1)
        self.assertEqual(self.game.score(), 29)
    
    def test_all_strikes(self):
        self.roll_many(12, 10)
        self.assertEqual(self.game.score(), 300)
    
    def test_strike_spare(self):
        self.roll_many(10, 5)
        self.roll_many(7, 10)
        self.assertEqual(self.game.score(), 230)

    def test_real_game(self):
        for pins in [10, 9, 1, 5, 5, 7, 2, 10, 10, 10, 9, 0, 8, 2, 9, 1, 10]:
            self.game.roll(pins)
        self.assertEqual(self.game.score(), 187)
