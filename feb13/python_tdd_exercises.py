def reverse_list(lst):
    """
    Reverses order of elements in list lst.
    """
    reversedlist = []
    for item in lst:
        reversedlist = [item] + reversedlist
    return reversedlist

def reverse_string(string):
    reversedstring = ''
    for item in string:
        reversedstring = item + reversedstring
    return reversedstring

def is_english_vowel(vowel):
    if vowel in 'aeiouyAEIOUY':
        return True
    return False

def count_num_vowels(s):
    counter = 0
    for v in s:
        if v in 'aeiouyAEIOUY':
            counter += 1
    return counter

def histogram(l):
    histogrm = ''
    for i, d in enumerate(l):
        histogrm += ('#' * d)
        if not (i + 1 == len(l)):
            histogrm += '\n'
    return histogrm

def get_word_lengths(s):
    counter = 0
    lengths = []
    for i in s:
        if i == ' ':
            lengths += [counter]
            counter = 0
        else:
            counter += 1
    lengths += [counter]
    return lengths

def find_longest_word(s):
    counter = 0
    lengths = []
    word = ''
    words = []
    for i in s:
        if i == ' ':
            if counter > max(lengths + [0]):
                longest = word
            lengths += [counter]
            counter = 0
            words += [word]
            word = ''
        else:
            counter += 1
            word += i
    lengths += [counter]
    words += [word]
    return longest


def validate_dna(dna):
    for i in dna:
        if not i in 'actgACTG':
            return False
    return True

def base_pair(c):
    dnamap = {'a':'t', 't':'a', 'g':'c', 'c':'g', 'A':'t', 'T':'a', 'G':'c', 'C':'g'}
    if c in dnamap:
        return dnamap[c]
    else:
        return 'unknown'

def transcribe_dna_to_rna(dna):
    validate_dna(dna)
    rnamap = {'a':'A', 't':'U', 'g':'G', 'c':'C', 'A':'A', 'T':'U', 'G':'G', 'C':'C'}
    rna = ''
    for i in dna:
        rna += rnamap[i]
    return rna

def get_complement(s):
    cmap = {'a':'T', 't':'A', 'g':'C', 'c':'G', 'A':'T', 'T':'A', 'G':'C', 'C':'G'}
    c = ''
    for i in s:
        c += cmap[i]
    return c

def get_reverse_complement(s):
    cmap = {'a':'T', 't':'A', 'g':'C', 'c':'G', 'A':'T', 'T':'A', 'G':'C', 'C':'G'}
    c = ''
    for i in s:
        c = cmap[i] + c
    return c

def remove_substring(ss, s):
    new = ''
    i = 0
    while i < len(s):
        d = 0
        matches = True
        while d < len(ss):
            if i + d >= len(s):
                matches = False
                d = 10000
            else:
                if not s[i + d] == ss[d]:
                    matches = False
            d += 1
        if matches:
            i += d
        else:
            new += s[i]
            i += 1
    return new

def get_position_indices(ss, s):
    new = ''
    positions = []
    i = 0
    while i < len(s):
        d = 0
        matches = True
        while d < len(ss):
            if i + d >= len(s):
                matches = False
                d = 10000
            else:
                if not s[i + d] == ss[d]:
                    matches = False
            d += 1
        if matches:
            positions += [(i/3)]
            i += d
        else:
            new += s[i]
            i += 1
    return positions

def get_3mer_usage_chart(s):
    result = []
    threemers = ['AAG', 'ACT', 'AGA', 'AGC', 'AGG', 'CCG', 'CGG', 'CTT', 'GAA', 'GAG', 'GCT', 'GGA', 'TAC', 'TAG', 'TTA']
    for threemer in threemers:
        amount = len(get_position_indices(threemer, s))
        result += [(threemer, amount)]
    return result

def read_column(file_name, column):
    f = open(file_name, "r")
    returnlist = []
    for i in f.readlines():
        s = 0
        l = 0
        while s < column - 1:
            while i[l] != " ":
                l += 1
            while i[l] == " ":
                l += 1
            s += 1
        k = l
        while i[k] != " ":
            k += 1
        returnlist += [""]
        returnlist[-1] += i[l:k]
        returnlist[-1] = float(returnlist[-1])
    return returnlist

def character_statistics(file_name):
    abundancies = [0]*256
    f = open(file_name, "r")
    for i in f.readlines():
        for j in i:
            if ord(j) > 64 and ord(j) < 91:
                abundancies[ord(j)+32] += 1
            else:
                abundancies[ord(j)] += 1            
    
    maxa = 0
    mina = 100000
    for a in range(97, 123):
        if (abundancies[a] > maxa) and not (chr(a) == " "):
            most_abundant = chr(a)
            maxa = abundancies[a]
        if abundancies[a] and (abundancies[a] < mina):
            least_abundant = chr(a)
            mina = abundancies[a]
    return most_abundant, least_abundant


def pythagorean_triples(n):
    """
    Returns list of all unique pythagorean triples
    (a, b, c) where a < b < c <= n.
    """
    l = []
    # loop over all a < b < c <= n
    for c in range(1, n + 1):
        for b in range(1, c):
            for a in range(1, b):
                if a*a + b*b == c*c:
                    l.append((a, b, c))
    return l


